{
  config,
  pkgs,
  ...
}: {
  imports = [
    <nixpkgs/nixos/modules/installer/cd-dvd/installation-cd-minimal.nix>
    <nixpkgs/nixos/modules/installer/cd-dvd/channel.nix>
    #../system/hardening.nix
  ];
  systemd.services."Pull configuration from git.guymatus.tech" = {
    serviceConfig.ExecStart = let
      script = pkgs.writeScript "pull-configs" ''
               #!${pkgs.bash}
        ${pkgs.git} clone https://git.guymatus.tech/TenTypekMatus/nixos-infra /provision
        sudo nixos-rebuild switch --flake .#sysdrive_format
      '';
    in "${script}";
  };
}
