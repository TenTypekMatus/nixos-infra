{
  config,
  pkgs,
  ...
}: {
  services.homed.enable = true;
  services.nscd.enable = true;
  users.users.matus = {
    isNormalUser = true;
    description = "Matúš Maštena";
    extraGroups = ["networkmanager" "wheel" "video" "audio" "docker"];
    shell = pkgs.fish;
  };
}
