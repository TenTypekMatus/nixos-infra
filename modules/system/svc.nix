{pkgs, ...}: {
  services.mpd = {
  enable = true;
  musicDirectory = "/home/matus/musik";

  # Optional:
  network.listenAddress = "any"; # if you want to allow non-localhost connections
  startWhenNeeded = true; # systemd feature: only start MPD service upon connection to its socket
};
hardware.pulseaudio.extraConfig = "load-module module-native-protocol-tcp auth-ip-acl=127.0.0.1";
  programs.nix-ld.enable = true;
  services = {
    xserver = {
      displayManager.sddm.enable = true;
      libinput.enable = true;
      enable = true;
      layout = "us";
      xkbVariant = "";
    };
    pantheon.apps.enable = false;
    flatpak = {
      enable = true;
    };
    gnome.gnome-keyring.enable = true;
    printing.enable = true;
  };
  networking.networkmanager.enable = true;
  virtualisation.docker.enable = true;
  security.polkit.enable = true;
  programs.light.enable = true;
  security.pam.services.swaylock.text = ''
    # PAM configuration file for the swaylock screen locker. By default, it includes
    # the 'login' configuration file (see /etc/pam.d/login)
    		auth include login
  '';
  virtualisation.libvirtd = {
    enable = true;
  };
    services.udev = {
    extraRules = ''
    ACTION=="add", KERNEL=="sd*[0-9]", ENV{ID_SERIAL}=="NOVATEKN_vt-DSC*", RUN+="${pkgs.systemd}/bin/systemctl --no-block start copyCamFiles@%k.service"
    '' ;
  };
  services.auto-cpufreq.enable = true;
  services.printing.drivers = [ pkgs.gutenprint ];
  nixpkgs.config.pulseaudio = true;
  xdg.portal.enable = true;
  security.rtkit.enable = true;
  hardware.pulseaudio = {
    enable = true;
  };
  programs.hyprland.enable = true;
  systemd = {
    user.services.polkit-gnome-authentication-agent-1 = {
      description = "polkit-gnome-authentication-agent-1";
      wantedBy = ["graphical-session.target"];
      wants = ["graphical-session.target"];
      after = ["graphical-session.target"];
      serviceConfig = {
        Type = "simple";
        ExecStart = "${pkgs.polkit_gnome}/libexec/polkit-gnome-authentication-agent-1";
        Restart = "on-failure";
        RestartSec = 1;
        TimeoutStopSec = 10;
      };
    };
  };
}
