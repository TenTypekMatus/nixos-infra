{...}: {
  services = {
    xserver = {
      displayManager.lightdm.enable = false;
      libinput.enable = true;
      enable = true;
      layout = "us";
      xkbVariant = "";
    };
    pantheon.apps.enable = false;
    flatpak = {
      enable = true;
    };
    gnome.gnome-keyring.enable = true;
    printing.enable = true;
  };
  networking.networkmanager.enable = true;
  virtualisation.docker.enable = true;
  security.polkit.enable = true;
  programs.light.enable = true;
  security.pam.services.swaylock.text = ''
    # PAM configuration file for the swaylock screen locker. By default, it includes
    # the 'login' configuration file (see /etc/pam.d/login)
    auth include login
  '';
  services.auto-cpufreq.enable = true;
  nixpkgs.config.pulseaudio = true;
  xdg.portal.enable = true;
  hardware.pulseaudio.enable = true;
}
