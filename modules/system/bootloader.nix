{
  config,
  pkgs,
  lib,
  ...
}: {
  boot = {
    bootspec.enable = true;
    loader = {
      grub.enable = lib.mkForce true;
      efi = {
        canTouchEfiVariables = true;
        efiSysMountPoint = "/boot"; # ← use the same mount point here.
      };
      grub = {
        efiSupport = true;
        #efiInstallAsRemovable = true; # in case canTouchEfiVariables doesn't work for your system
        device = "nodev";
      };
    };
  };
}
