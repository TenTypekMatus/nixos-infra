{
  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-unstable";
    home-manager.url = "github:nix-community/home-manager";
    lanzaboote.url = "github:nix-community/lanzaboote";
    disko.url = "github:nix-community/disko";
    home-manager.inputs.nixpkgs.follows = "nixpkgs";
  };

  outputs = {
    self,
    nixpkgs,
    lanzaboote,
    home-manager,
    disko,
    ...
  } @ inputs: {
    formatter.x86_64-linux = nixpkgs.legacyPackages.x86_64-linux.nixpkgs-fmt;

    nixosConfigurations.LinUwU = inputs.nixpkgs.lib.nixosSystem {
      system = "x86_64-linux";
      specialArgs.inputs = inputs;
      modules = [
        home-manager.nixosModules.home-manager
        lanzaboote.nixosModules.lanzaboote
        {
          home-manager.useGlobalPkgs = true;
          home-manager.useUserPackages = true;
          home-manager.users.matus = import ./home-manager/home.nix;
        }
	./modules/system/secureboot.nix
        ./modules/system/other-repos.nix
        ./configuration-laptop.nix
        ./modules/system/torbrowser.nix
      ];
    };

    nixosConfigurations.MatuusPC = inputs.nixpkgs.lib.nixosSystem {
      system = "x86_64-linux";
      specialArgs.inputs = inputs;
      modules = [
        home-manager.nixosModules.home-manager
        {
          home-manager.useGlobalPkgs = true;
          home-manager.useUserPackages = true;
          home-manager.users.matus = import ./home-manager/home.nix;
        }
        ./modules/system/other-repos.nix
        ./configuration.nix
        ./hardware-configuration.nix
        ./modules/system/nvidia.nix
        ./modules/system/torbrowser.nix
      ];
    };
    nixosConfigurations.MatuusAIO = inputs.nixpkgs.lib.nixosSystem {
      system = "x86_64-linux";
      specialArgs.inputs = inputs;
      modules = [
        home-manager.nixosModules.home-manager
        {
          home-manager.useGlobalPkgs = true;
          home-manager.useUserPackages = true;
          home-manager.users.matus = import ./home-manager/home.nix;
        }
        ./modules/system/other-repos.nix
        ./configuration-aio.nix
        ./hw-configuration-aio.nix
        ./modules/system/nvidia.nix
        ./modules/system/torbrowser.nix
      ];
    };
  };
}
