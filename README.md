# Infra for my devices running NixOS

My personal NixOS infrastructure for my laptop, tower PC with Nvidia and AiO PC also with Nvidia.

# Installation

> :warning: Warning: Do NOT use this on your system!!! Your use cases may differ. You need to also pull [this repo](https://git.guymatus.tech/TenTypekMatus/dots-typekmatus) to make it work properly.

- Clone this repo
- Run `sudo nixos-rebuild switch --flake .#<name of the device>`
- Wait and reboot

# Some nice screenshots

<img title="" src="pic/Screenshot%20from%202023-07-25%2013-49-35.png" alt="" data-align="inline"><img title="" src="pic/Screenshot from 2023-07-25 14-10-52.png" alt="" data-align="inline">

<img title="" src="pic/Screenshot from 2023-07-25 13-49-48.png" alt="" data-align="inline">
