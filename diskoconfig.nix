{lib, ...}: {
  disko.devices.disk = lib.genAttrs ["/dev/sda"] (disk: {
    type = "disk";
    device = disk;
    content = {
      type = "table";
      format = "gpt";
      partitions = [
        {
          name = "boot";
          start = "0";
          end = "1M";
          part-type = "primary";
          flags = ["bios_grub"];
        }
        {
          name = "ESP";
          start = "1M";
          end = "1047MB";
          fs-type = "fat32";
          bootable = true;
          content = {
            type = "filesystem";
            format = "vfat";
            mountpoint = "/boot";
          };
        }
        {
          name = "root";
          start = "1074MB";
          end = "100%";
          part-type = "primary";
          bootable = true;
          content = {
            type = "luks";
            name = "rootfs";
            extraOpenArgs = ["--allow-discards"];
            keyFile = "/key/hdd.key";
            content = {
              type = "filesystem";
              format = "xfs";
              mountpoint = "/";
            };
          };
        }
      ];
    };
  });
}
