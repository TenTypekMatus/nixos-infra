{
  pkgs,
  lib,
  ...
}: {
  imports = [
    ./pkgs.nix
    ./modules/nvim.nix
  ];

  programs.chromium = {
    enable = true;
    package = pkgs.google-chrome;
  };
  home = {
    username = "matus";
    homeDirectory = "/home/matus";
    stateVersion = "23.11";
  };

  gtk = {
    enable = true;
    theme = {
      package = pkgs.tokyo-night-gtk;
      name = "Tokyonight-Dark-B";
    };
    iconTheme = {
      package = pkgs.papirus-icon-theme;
      name = "Papirus-Dark";
    };
    font = {
      name = "Sans";
      size = 11;
    };
  };
  services.betterlockscreen.enable = true;
  nixpkgs.config.allowUnfree = true;
  # The home.packages option allows you to install Nix packages into your
  # environment.
  programs.git = {
    enable = true;
    userName = "Matus Mastena";
    package = pkgs.git;
    userEmail = "Shadiness9530@proton.me";
    extraConfig = {
      credential.helper = "${
        pkgs.git.override {withLibsecret = true;}
      }/bin/git-credential-libsecret";
    };
  };
  programs.vscode = {
    enable = true;
    package = pkgs.vscodium.fhs;
    extensions = with pkgs.vscode-extensions; [
      pkief.material-product-icons
      pkief.material-icon-theme
      arcticicestudio.nord-visual-studio-code
    ];
  };
  programs.tmux = {
    enable = true;
    extraConfig = ''
      set -g status-right '  #{cpu_percentage} #{battery_icon} #{battery_percentage}  %H:%M '
      run-shell ${pkgs.tmuxPlugins.cpu}/share/tmux-plugins/cpu/cpu.tmux
      run-shell ${pkgs.tmuxPlugins.battery}/share/tmux-plugins/battery/battery.tmux
      unbind C-Space
      set -g prefix C-Space
      set -g mouse on
      bind C-Space send-prefix
      bind h split-window -hc "#{pane_current_path}"
      bind v split-window -vc "#{pane_current_path}"
    '';
  };

  nixpkgs.config.allowUnfreePredicate = pkg:
    builtins.elem (lib.getName pkg) [
      "discord"
      "spotify"
    ];

  home.sessionVariables = {
    EDITOR = "nvim";
    MANPAGER="nvim +Man!";
  };

  # Let Home Manager install and manage itself.
  programs.home-manager.enable = true;
}
