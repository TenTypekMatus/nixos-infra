# Purely functional Neovim config
{pkgs, ...}: {
  programs.neovim = {
    enable = true;
    plugins = with pkgs.vimPlugins; [
      vim-autoformat
      vimsence
      vim-fugitive
      vim-flog
      barbecue-nvim
      tokyonight-nvim
      toggleterm-nvim
      nvim-dap
      nvim-dap-ui
      neodev-nvim
      formatter-nvim
      nvim-lint
      nvim-navic
      mason-nvim
      mason-lspconfig-nvim
      ultisnips
      vim-snippets
      vim-devicons
      ansible-vim
      nvim-treesitter
      nvim-treesitter.withAllGrammars
      nvim-web-devicons
      tabby-nvim
      cmp-nvim-lsp
      cmp-buffer
      cmp-path
      nvim-cmp
      lspkind-nvim
      cmp-nvim-ultisnips
      nvim-lspconfig
      auto-save-nvim
      nvim-autopairs
      telescope-nvim
      which-key-nvim
      plenary-nvim
      todo-comments-nvim
      trouble-nvim
      wilder-nvim
      lualine-nvim
      rust-tools-nvim
      nvim-tree-lua
      dashboard-nvim
      nvim-colorizer-lua
    ];
    extraConfig = ''
      set nobackup
      set nowritebackup
      set nowildmenu
      set updatetime=300
      let g:barbar_auto_setup=v:true
      set signcolumn=yes
      packadd! nvim-colorizer.lua
      lua <<EOF
      require("lualine").setup()
      require 'colorizer'.setup()
      EOF
      let g:vimsence_client_id = '1163509194335457280'
      lua <<EOF
      local cmp = require'cmp'
      local lspkind = require'lspkind'
      cmp.setup({
        snippet = {
                expand = function(args)
                        -- For `ultisnips` user.
                        vim.fn["UltiSnips#Anon"](args.body)
                end,
        },
        mapping = cmp.mapping.preset.insert({
                ['<Tab>'] = function(fallback)
                        if cmp.visible() then
                                cmp.select_next_item()
                        else
                                fallback()
                        end
                end,
                ['<S-Tab>'] = function(fallback)
                        if cmp.visible() then
                                cmp.select_prev_item()
                        else
                                fallback()
                        end
                end,
                ['<CR>'] = cmp.mapping.confirm({ select = true }),
                ['<C-e>'] = cmp.mapping.abort(),
                ['<Esc>'] = cmp.mapping.close(),
                ['<C-d>'] = cmp.mapping.scroll_docs(-4),
                ['<C-f>'] = cmp.mapping.scroll_docs(4),
        }),
        sources = {
                { name = 'nvim_lsp' }, -- For nvim-lsp
                { name = 'ultisnips' }, -- For ultisnips user.
                { name = 'nvim_lua' }, -- for nvim lua function
                { name = 'path' }, -- for path completion
                { name = 'buffer', keyword_length = 4 }, -- for buffer word completion
                { name = 'omni' },
                { name = 'emoji', insert = true, } -- emoji completion
        },
        window = {
                completion = cmp.config.window.bordered(),
                documentation = cmp.config.window.bordered(),
        },
      completion = {
                keyword_length = 1,
                completeopt = "menu,noselect"
        },
        view = {
                entries = 'custom',
        },
        formatting = {
                format = lspkind.cmp_format({
                        mode = "symbol_text",
                        menu = ({
                                nvim_lsp = "[LSP]",
                                ultisnips = "[US]",
                                nvim_lua = "[Lua]",
                                path = "[Path]",
                                buffer = "[Buffer]",
                                emoji = "[Emoji]",
                                omni = "[Omni]",
                        }),
                }),
        },
      })
      local rt = require("rust-tools")

      rt.setup({
      server = {
      on_attach = function(_, bufnr)
      -- Hover actions
      vim.keymap.set("n", "<C-space>", rt.hover_actions.hover_actions, { buffer = bufnr })
      -- Code action groups
      vim.keymap.set("n", "<Leader>a", rt.code_action_group.code_action_group, { buffer = bufnr })
      end,
      },
      })
      EOF
      lua require("toggleterm").setup()
      lua require("which-key").setup()
      nnoremap <silent><c-t> <Cmd>exe v:count1 . "ToggleTerm"<CR>
      inoremap <silent><c-t> <Esc><Cmd>exe v:count1 . "ToggleTerm"<CR>
      call wilder#setup({'modes': [':', '/', '?']})
      call wilder#set_option('renderer', wilder#popupmenu_renderer(wilder#popupmenu_palette_theme({
                        \ 'highlighter': wilder#basic_highlighter(),
                        \ 'highlights': {
                        \   'accent': wilder#make_hl('WilderAccent', 'Pmenu', [{}, {}, {'foreground': '#f4468f'}]),
                        \ },
                        \ 'reverse': 0,
                        \ 'left': [
                        \   ' ', wilder#popupmenu_devicons(),
                        \ ],
                        \ 'right': [
                        \   ' ', wilder#popupmenu_scrollbar(),
                        \ ],
                        \ })))
      lua require("nvim-tree").setup()
      lua require("mason").setup()
      lua require("todo-comments").setup()
      lua <<EOF
      require("dashboard").setup({
      theme = 'hyper',
      config = {
      week_header = {
      enable = true,
      },
      shortcut = {
      {
        icon = ' ',
        icon_hl = '@variable',
        desc = 'Files',
        group = 'Label',
        action = 'Telescope find_files',
        key = 'f',
        },
        {
                desc = ' Projects',
                group = 'DiagnosticHint',
                action = 'Telescope ~/proj',
                key = 'a',
                },
                {
                        desc = ' dotfiles',
                        group = 'Number',
                        action = 'Telescope dotfiles',
                        key = 'd',
                        },
                },
      },
      })
      EOF
      lua <<EOF
      vim.o.showtabline = 2
      local theme = {
      fill = 'TabLineFill',
      -- Also you can do this: fill = { fg='#f2e9de', bg='#907aa9', style='italic' }
      head = 'TabLine',
      current_tab = 'TabLineSel',
      tab = 'TabLine',
      win = 'TabLine',
      tail = 'TabLine',
      }
      require('tabby.tabline').set(function(line)
      return {
      {
      { '  ', hl = theme.head },
      line.sep('', theme.head, theme.fill),
      },
      line.tabs().foreach(function(tab)
      local hl = tab.is_current() and theme.current_tab or theme.tab
      return {
        line.sep('', hl, theme.fill),
        tab.is_current() and '' or '󰆣',
        tab.number(),
        tab.name(),
        tab.close_btn(''),
        line.sep('', hl, theme.fill),
        hl = hl,
        margin = ' ',
      }
      end),
      line.spacer(),
      line.wins_in_tab(line.api.get_current_tab()).foreach(function(win)
      return {
        line.sep('', theme.win, theme.fill),
        win.is_current() and '' or '',
        win.buf_name(),
        line.sep('', theme.win, theme.fill),
        hl = theme.win,
        margin = ' ',
      }
      end),
      {
      line.sep('', theme.tail, theme.fill),
      { '  ', hl = theme.tail },
      },
      hl = theme.fill,
      }
      end)
      EOF
      lua <<EOF
      require("mason-lspconfig").setup {
      ensure_installed = { "lua_ls", "vls", "clangd", "rust_analyzer", "nil_ls", "bashls", "ast_grep", "ansiblels", "docker_compose_language_service" },
      }
      EOF
      lua vim.loader.enable()
      lua require("trouble").setup()
      lua require("nvim-autopairs").setup{}
      lua <<EOF
      require("neodev").setup({
      library = { plugins = { "nvim-dap-ui" }, types = true },
      })
      -- triggers CursorHold event faster
      vim.opt.updatetime = 200

      require("barbecue").setup({
      create_autocmd = true, -- prevent barbecue from updating itself automatically
      })

      vim.api.nvim_create_autocmd({
      "WinScrolled", -- or WinResized on NVIM-v0.9 and higher
      "BufWinEnter",
      "CursorHold",
      "InsertLeave",

      -- include this if you have set `show_modified` to `true`
      "BufModifiedSet",
      }, {
      group = vim.api.nvim_create_augroup("barbecue.updater", {}),
      callback = function()
      require("barbecue.ui").update()
      end,
      })
      EOF
      colorscheme tokyonight-night
      lua require('lspconfig')['ansiblels'].setup {}
      lua require('lspconfig')['docker-compose-language-service '].setup {}
      lua require'lspconfig'.vls.setup {}
      lua require'lspconfig'.clangd.setup {}
      lua require('lspconfig')['rust_analyzer'].setup {}
      lua require('lspconfig')['bashls'].setup {}
      lua require('lspconfig')['nil_ls'].setup {}
      set noshowmode
      set mouse=a
      set number
      set encoding=utf-8
      let mapleader = "\<Space>"
      nnoremap <leader>nt :tabnew<CR>
      nnoremap <leader>tn :tabnext<CR>
      nnoremap <leader>tc :tabclose<CR>
      nnoremap <leader>ff :NvimTreeToggle<CR>
    '';
    viAlias = true;
    vimAlias = true;
  };
}
