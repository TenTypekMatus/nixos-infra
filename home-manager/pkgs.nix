{pkgs, ...}: {
  home.packages = with pkgs; [
    (nerdfonts.override {fonts = ["Agave"];})
    pmbootstrap # For porting my phone
    gnome.gnome-screenshot
    protonmail-bridge # For mails
    libreoffice
    htop # System monito
    obsidian # The best browser under the sun
    acpi # For the battery
    python311Packages.pynvim
    clang # C compiler
    _1password-gui # Password manager
    _1password # His CLI version
    yubikey-personalization-gui # Yubikey settings manager
    rofi-power-menu
    jetbrains.rust-rover
    swaybg
    r2modman
    swayidle
    swaylock-fancy
    pipx
    airshipper
    qemu
    gnome.gnome-calculator
    gamemode
    clang-tools_17
    filezilla
    thunderbird
    xsel
    deluge
    jetbrains-toolbox
    freetube
    ansible
    ansible-lint
    nodejs
    python3
    xfce.thunar
    xfce.xfce4-session
    bat
    python311Packages.pynvim
    xorg.xprop
    unzip
    lollypop
    discord
    alacritty
    pavucontrol
    autotiling
    starship
    fish
    eza
    neofetch
    musikcube
    papirus-icon-theme
    tokyo-night-gtk
    vlang
    prismlauncher
    rustup
    noto-fonts-emoji
  ];
}
